Star Trails
===========

Like most of the projects in my GitHub, Star Trails is self-educatory, and provided here for free viewing in case any of my fellow developers out there needs to see an example of a particular technology in action. In this case, I'm creating an application as a test of location-aware search using Hibernate.

You can view the actual Star Trails web application [here](http://www.nijhazer.com/star-trails).