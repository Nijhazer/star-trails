package com.nijhazer.startrails.service;

import com.nijhazer.startrails.model.entity.Place;
import com.nijhazer.startrails.model.entity.ServiceResponse;
import com.nijhazer.startrails.model.manager.PlaceManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Author: M.C. Wilson
 * Date:   10/1/12
 * Time:   4:12 PM
 */
@Path("places")
public class PlaceService {
    private static final Logger logger = LoggerFactory.getLogger(PlaceService.class);
    private PlaceManager placeManager;

    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<Place> getPlaces() {
        return placeManager.findAll();
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("{name}")
    public Response getPlaceByName(@PathParam("name") String name) {
        return Response.ok(placeManager.findByName(name)).build();
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("search")
    public List<Place> findPlacesByDistance(
            @QueryParam("latitude") Double latitude,
            @QueryParam("longitude") Double longitude,
            @QueryParam("radius") Double radius) {
        return placeManager.findByDistanceFrom(latitude, longitude, radius);
    }

    @POST
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response createPlace(
        @FormParam("name") String name,
        @FormParam("description") String description,
        @FormParam("address") String address,
        @FormParam("latitude") Double latitude,
        @FormParam("longitude") Double longitude) throws Exception {
        logger.debug("Received a Create Place request. Name: {} - Description: {} - Address: {} - Latitude: {} - Longitude: {}",
            new Object[] {
                name,
                description,
                address,
                latitude,
                longitude
            });
        Place place = new Place();
        place.setName(name);
        place.setDescription(description);
        if (latitude != null && longitude != null) {
            place = placeManager.create(place, latitude, longitude);
        } else {
            place = placeManager.create(place, address);
        }
        return Response.ok(place).build();
    }

    @PUT
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("search/index/{id}")
    public Response indexPlace(@PathParam("id") Long placeId) {
        logger.debug("Indexing place {}", placeId);
        placeManager.addToIndex(placeId);
        return Response.ok().build();
    }

    @PUT
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("{name}")
    public Response updatePlace(
            @PathParam("name") String name,
            @FormParam("description") String description,
            @FormParam("address") String address,
            @FormParam("latitude") Double latitude,
            @FormParam("longitude") Double longitude) throws Exception {
        logger.debug("Received an Update Place request. Name: {} - Description: {} - Address: {} - Latitude: {} - Longitude: {}",
            new Object[] {
                name,
                description,
                address,
                latitude,
                longitude
            });
        Place place = new Place();
        place.setName(name);
        place.setDescription(description);
        if (latitude != null && longitude != null) {
            place = placeManager.update(place, latitude, longitude);
        } else {
            place = placeManager.update(place, address);
        }
        return Response.ok(place).build();
    }

    @DELETE
    @Path("{name}")
    public Response deletePlace(@PathParam("name") String name) {
        Place place = new Place();
        place.setName(name);
        placeManager.delete(place);
        return Response.ok().build();
    }

    public void setPlaceManager(PlaceManager placeManager) {
        this.placeManager = placeManager;
    }
}
