package com.nijhazer.startrails.service.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.TransactionSystemException;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Author: M.C. Wilson
 * Date:   10/2/12
 * Time:   12:14 PM
 */
@Provider
public class TransactionSystemExceptionMapper implements ExceptionMapper<TransactionSystemException> {
    private static final Logger logger = LoggerFactory.getLogger(TransactionSystemExceptionMapper.class);

    @Context
    private HttpHeaders headers;

    public Response toResponse(TransactionSystemException e) {
        logger.error("Exception while processing transaction within REST layer - input: {}",
                headers.getRequestHeaders(),
                e.getRootCause());
        return Response
                .status(Response.Status.INTERNAL_SERVER_ERROR)
                .type(headers.getMediaType())
                .entity(e.getRootCause().getMessage())
                .build();
    }
}
