package com.nijhazer.startrails.service.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Author: M.C. Wilson
 * Date:   10/2/12
 * Time:   12:14 PM
 */
@Provider
public class DataIntegrityViolationExceptionMapper implements ExceptionMapper<DataIntegrityViolationException> {
    private static final Logger logger = LoggerFactory.getLogger(DataIntegrityViolationExceptionMapper.class);

    @Context
    private HttpHeaders headers;

    public Response toResponse(DataIntegrityViolationException e) {
        logger.debug("Requested operation would violate data integrity", e);
        return Response
                .status(Response.Status.CONFLICT)
                .type(headers.getMediaType())
                .entity(e.getMessage())
                .build();
    }
}
