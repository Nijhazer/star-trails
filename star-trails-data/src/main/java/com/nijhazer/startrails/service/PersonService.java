package com.nijhazer.startrails.service;

import com.nijhazer.startrails.model.entity.Person;
import com.nijhazer.startrails.model.manager.PersonManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Author: M.C. Wilson
 * Date:   10/1/12
 * Time:   4:12 PM
 */
@Path("people")
public class PersonService {
    private static final Logger logger = LoggerFactory.getLogger(PersonService.class);
    private PersonManager personManager;

    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<Person> getPeople() {
        return personManager.findAll();
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("{email}")
    public Response getPersonByEmail(@PathParam("email") String email) {
        return Response.ok(personManager.findByEmail(email)).build();
    }

    @POST
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response createPerson(
        @FormParam("email") String email,
        @FormParam("firstName") String firstName,
        @FormParam("lastName") String lastName,
        @FormParam("address") String address) throws Exception {
        Person person = new Person();
        person.setEmail(email);
        person.setFirstName(firstName);
        person.setLastName(lastName);
        return Response.ok(personManager.create(person, address)).build();
    }

    @PUT
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("{email}")
    public Response updatePerson(
            @PathParam("email") String email,
            @FormParam("firstName") String firstName,
            @FormParam("lastName") String lastName,
            @FormParam("address") String address) throws Exception {
        Person person = new Person();
        person.setEmail(email);
        person.setFirstName(firstName);
        person.setLastName(lastName);
        return Response.ok(personManager.update(person, address)).build();
    }

    @DELETE
    @Path("{email}")
    public Response deletePerson(@PathParam("email") String email) {
        Person person = new Person();
        person.setEmail(email);
        personManager.delete(person);
        return Response.ok().build();
    }

    public void setPersonManager(PersonManager personManager) {
        this.personManager = personManager;
    }
}
