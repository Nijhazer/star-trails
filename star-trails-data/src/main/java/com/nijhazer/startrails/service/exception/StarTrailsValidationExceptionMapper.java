package com.nijhazer.startrails.service.exception;

import com.nijhazer.startrails.model.exception.StarTrailsValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Author: M.C. Wilson
 * Date:   10/2/12
 * Time:   12:14 PM
 */
@Provider
public class StarTrailsValidationExceptionMapper implements ExceptionMapper<StarTrailsValidationException> {
    private static final Logger logger = LoggerFactory.getLogger(StarTrailsValidationExceptionMapper.class);

    @Context
    private HttpHeaders headers;

    public Response toResponse(StarTrailsValidationException e) {
        logger.debug("Unable to validate input", e);
        return Response
                .status(Response.Status.PRECONDITION_FAILED)
                .type(headers.getMediaType())
                .entity(e.getMessage())
                .build();
    }
}
