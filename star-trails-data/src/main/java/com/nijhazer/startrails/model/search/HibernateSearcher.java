package com.nijhazer.startrails.model.search;

import org.apache.commons.lang.StringUtils;
import org.hibernate.search.jpa.Search;
import org.hibernate.search.spatial.SpatialQueryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class HibernateSearcher<K, T> implements JpaSearcher<K, T> {
    private static final Logger logger = LoggerFactory.getLogger(HibernateSearcher.class);

    @PersistenceContext
    private EntityManager entityManager;

    private String gridCollectionName;

    @SuppressWarnings("unchecked")
    @Transactional
    public List<T> findByDistanceFrom(Double latitude, Double longitude, Double radius, Class clazz) {
        logger.debug("Entity Manager: {}", entityManager);

        org.apache.lucene.search.Query luceneQuery =
            SpatialQueryBuilder.buildSpatialQueryByGrid(
                latitude,
                longitude,
                radius,
                getGridCollectionName()
            );

        return (List<T>) Search
            .getFullTextEntityManager(entityManager)
            .createFullTextQuery(luceneQuery, clazz)
            .getResultList();
    }

    @SuppressWarnings("unchecked")
    @Transactional
    public void addToIndex(K id, Class clazz) {
        T jpaObject =
                (T) entityManager
                        .createQuery("SELECT a FROM " + clazz.getSimpleName() + " a WHERE a.id = :id")
                        .setParameter("id", id)
                        .getSingleResult();
        Search.getFullTextEntityManager(entityManager).index(jpaObject);
    }

    public String getGridCollectionName() {
        return gridCollectionName;
    }

    public void setGridCollectionName(String gridCollectionName) {
        this.gridCollectionName = gridCollectionName;
    }
}
