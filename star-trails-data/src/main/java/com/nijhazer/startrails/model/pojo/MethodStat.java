package com.nijhazer.startrails.model.pojo;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.concurrent.TimeUnit;

/**
 * Author: M.C. Wilson
 * Date:   10/23/12
 * Time:   10:35 PM
 */
@XmlRootElement
public class MethodStat implements Serializable {
    private String methodName;
    private boolean started;
    private long startTime;
    private long endTime;
    private long elapsedTime;

    public MethodStat() { }

    public MethodStat(String methodName) {
        this.setMethodName(methodName);
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public long getElapsedTime() {
        return elapsedTime;
    }

    public void setElapsedTime(long elapsedTime) {
        this.elapsedTime = elapsedTime;
    }

    public void start() {
        this.started = true;
        this.setStartTime(System.nanoTime());
    }

    public void finish() {
        this.setEndTime(System.nanoTime());
        if (this.started) {
            this.setElapsedTime(this.getEndTime() - this.getStartTime());
        }
    }

    @Override
    public String toString() {
        if (this.started) {
            return " ["
                   + this.getMethodName()
                   + "] Started: "
                   + this.getStartTime()
                   + ", Ran: "
                   + TimeUnit.MILLISECONDS.convert(this.getElapsedTime(), TimeUnit.NANOSECONDS)
                   + " ms";
        }
        return "[" + this.getMethodName() + "] [Incomplete Method Statistic]";
    }
}
