package com.nijhazer.startrails.model.entity.base;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

/**
 * Author: M.C. Wilson
 * Date:   10/4/12
 * Time:   3:51 PM
 */
public class AbstractObject implements Serializable {
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
