package com.nijhazer.startrails.model.dao;

import org.springframework.transaction.annotation.Transactional;

/**
 * Author: M.C. Wilson
 * Date:   10/5/12
 * Time:   2:18 PM
 */
public interface SpringDao<T, K> extends Dao<T, K> {
    @Transactional
    public T create(T object);

    @Transactional
    public T update(T object);

    @Transactional
    public void delete(T object);
}
