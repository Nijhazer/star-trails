package com.nijhazer.startrails.model.validator;

import com.nijhazer.startrails.model.entity.Person;
import com.nijhazer.startrails.model.exception.StarTrailsValidationException;
import com.nijhazer.startrails.util.ValidationExceptionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Set;

/**
 * Author: M.C. Wilson
 * Date:   10/2/12
 * Time:   3:11 PM
 */
public class PersonValidator {
    private static final Logger logger = LoggerFactory.getLogger(PersonValidator.class);
    private Validator validator;
    private ValidationExceptionUtil<Person> veu = new ValidationExceptionUtil<Person>();

    public void validate(Person person) throws StarTrailsValidationException {
        logger.debug("Validating Person: {}", person);
        Set<ConstraintViolation<Person>> constraintViolations = validator.validate(person);

        if (constraintViolations.size() > 0) {
            logger.error("Validation failed: {}", constraintViolations);
            throw new StarTrailsValidationException(veu.format(constraintViolations));
        }
        logger.debug("Validation OK");
    }

    public void setValidator(Validator validator) {
        this.validator = validator;
    }
}
