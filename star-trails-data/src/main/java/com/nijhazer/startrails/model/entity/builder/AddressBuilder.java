package com.nijhazer.startrails.model.entity.builder;

import com.nijhazer.startrails.model.entity.Address;

/**
 * Author: M.C. Wilson
 * Date:   10/5/12
 * Time:   3:34 PM
 */
public class AddressBuilder implements ObjectBuilder<Address> {
    private Long id;
    private String streetNumber;
    private String route;
    private String city;
    private String county;
    private String state;
    private String nation;
    private String zipCode;
    private Double latitude;
    private Double longitude;

    public AddressBuilder() {
    }

    public Address build() {
        Address address = new Address();
        address.setId(id);
        address.setStreetNumber(streetNumber);
        address.setRoute(route);
        address.setCity(city);
        address.setCounty(county);
        address.setState(state);
        address.setNation(nation);
        address.setZipCode(zipCode);
        address.setLatitude(latitude);
        address.setLongitude(longitude);
        return address;
    }

    public AddressBuilder copyFrom(Address source) {
        this.id(source.getId())
            .streetNumber(source.getStreetNumber())
            .route(source.getRoute())
            .city(source.getCity())
            .county(source.getCounty())
            .state(source.getState())
            .nation(source.getNation())
            .zipCode(source.getZipCode())
            .latitude(source.getLatitude())
            .longitude(source.getLongitude());
        return this;
    }

    public AddressBuilder id(Long id) {
        this.id = id;
        return this;
    }

    public AddressBuilder streetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
        return this;
    }

    public AddressBuilder route(String route) {
        this.route = route;
        return this;
    }

    public AddressBuilder city(String city) {
        this.city = city;
        return this;
    }

    public AddressBuilder county(String county) {
        this.county = county;
        return this;
    }

    public AddressBuilder state(String state) {
        this.state = state;
        return this;
    }

    public AddressBuilder nation(String nation) {
        this.nation = nation;
        return this;
    }

    public AddressBuilder zipCode(String zipCode) {
        this.zipCode = zipCode;
        return this;
    }

    public AddressBuilder latitude(Double latitude) {
        this.latitude = latitude;
        return this;
    }

    public AddressBuilder longitude(Double longitude) {
        this.longitude = longitude;
        return this;
    }
}
