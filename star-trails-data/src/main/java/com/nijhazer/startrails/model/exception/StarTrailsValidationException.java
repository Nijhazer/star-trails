package com.nijhazer.startrails.model.exception;

import javax.validation.ValidationException;

/**
 * Author: M.C. Wilson
 * Date:   10/2/12
 * Time:   3:16 PM
 */
public class StarTrailsValidationException extends ValidationException {
    public StarTrailsValidationException(String message) {
        super(message);
    }
}
