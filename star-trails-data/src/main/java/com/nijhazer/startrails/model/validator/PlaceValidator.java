package com.nijhazer.startrails.model.validator;

import com.nijhazer.startrails.model.entity.Place;
import com.nijhazer.startrails.model.exception.StarTrailsValidationException;
import com.nijhazer.startrails.util.ValidationExceptionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Set;

/**
 * Author: M.C. Wilson
 * Date:   10/2/12
 * Time:   3:11 PM
 */
public class PlaceValidator {
    private static final Logger logger = LoggerFactory.getLogger(PlaceValidator.class);
    private Validator validator;
    private ValidationExceptionUtil<Place> veu = new ValidationExceptionUtil<Place>();

    public void validate(Place place) throws StarTrailsValidationException {
        logger.debug("Validating Place: {}", place);
        Set<ConstraintViolation<Place>> constraintViolations = validator.validate(place);

        if (constraintViolations.size() > 0) {
            logger.error("Validation failed: {}", constraintViolations);
            throw new StarTrailsValidationException(veu.format(constraintViolations));
        }
        logger.debug("Validation OK");
    }

    public void setValidator(Validator validator) {
        this.validator = validator;
    }
}
