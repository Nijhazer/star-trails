package com.nijhazer.startrails.model.manager;

import com.nijhazer.startrails.model.dao.PlaceDao;
import com.nijhazer.startrails.model.entity.Place;
import com.nijhazer.startrails.model.search.JpaSearcher;
import com.nijhazer.startrails.model.validator.PlaceValidator;
import com.nijhazer.startrails.util.formatter.AddressFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Author: M.C. Wilson
 * Date:   10/4/12
 * Time:   12:32 PM
 */
public class PlaceManager {
    private static final Logger logger = LoggerFactory.getLogger(PlaceManager.class);
    private PlaceValidator validator;
    private AddressFormatter formatter;
    private PlaceDao placeDao;
    private JpaSearcher<Long, Place> searcher;

    @PersistenceContext
    private EntityManager entityManager;

    public Place findByName(String name) {
        return placeDao.findByName(name);
    }

    public List<Place> findAll() {
       return placeDao.findAll();
    }

    public Place findByKey(Long key) {
        return placeDao.findByKey(key);
    }

    public List<Place> findByDistanceFrom(Double latitude, Double longitude, Double radius) {
        return searcher.findByDistanceFrom(latitude, longitude, radius, Place.class);
    }

    public Place create(Place place, String address) {
        place.setAddress(formatter.fromSingleAddressString(address));
        validator.validate(place);
        return create(place);
    }

    public Place create(Place place, Double latitude, Double longitude) {
        place.setAddress(formatter.fromLatitudeAndLongitude(latitude, longitude));
        validator.validate(place);
        return create(place);
    }

    public Place create(Place place) {
        validator.validate(place);
        return placeDao.create(place);
    }

    public Place update(Place place, String address) {
        place.setAddress(formatter.fromSingleAddressString(address));
        return update(place);
    }

    public Place update(Place place, Double latitude, Double longitude) {
        place.setAddress(formatter.fromLatitudeAndLongitude(latitude, longitude));
        return update(place);
    }

    public Place update(Place place) {
        Place placeToUpdate = findByName(place.getName());
        placeToUpdate.copyFromSource(place);

        validator.validate(place);
        return placeDao.update(place);
    }

    public void delete(Place place) {
        placeDao.delete(place);
    }

    public void setFormatter(AddressFormatter formatter) {
        this.formatter = formatter;
    }

    public void setPlaceDao(PlaceDao placeDao) {
        this.placeDao = placeDao;
    }

    public void setValidator(PlaceValidator validator) {
        this.validator = validator;
    }

    public void setSearcher(JpaSearcher<Long, Place> searcher) {
        this.searcher = searcher;
    }

    public void addToIndex(Long placeId) {
        searcher.addToIndex(placeId, Place.class);
    }
}
