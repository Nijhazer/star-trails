package com.nijhazer.startrails.model.dao;

import com.nijhazer.startrails.model.entity.Person;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Author: M.C. Wilson
 * Date:   10/2/12
 * Time:   2:17 PM
 */
public class JpaPersonDaoImpl implements PersonDao {
    private static final Logger logger = LoggerFactory.getLogger(JpaPersonDaoImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @SuppressWarnings("unchecked")
    public List<Person> findAll() {
        logger.debug("Retrieving all available Person entities");
        return entityManager
                .createQuery("SELECT a FROM Person a")
                .getResultList();
    }

    public Person findByKey(Long key) {
        logger.debug("Retrieving person with ID {}", key);
        return entityManager
                .find(Person.class, key);
    }

    public Person findByEmail(String email) {
        logger.debug("Retrieving entity associated with email address {}", email);
        return (Person) entityManager
                .createQuery("SELECT a FROM Person a WHERE a.email = :email")
                .setParameter("email", email)
                .getSingleResult();
    }

    @SuppressWarnings("unchecked")
    public List<Person> findByCityAndState(String city, String state) {
        return entityManager
                .createQuery("SELECT a FROM Person a WHERE a.address.city = :city AND a.address.state = :state")
                .setParameter("city", city)
                .setParameter("state", state)
                .getResultList();
    }

    public Person create(Person person) {
        logger.debug("Creating new person: {}", person);
        entityManager.persist(person);

        return person;
    }

    public Person update(Person person) {
        Person personToUpdate = findByEmail(person.getEmail());

        logger.debug("Old entity: {} - New entity: {}", personToUpdate, person);
        personToUpdate.copyFromSource(person);

        entityManager.merge(personToUpdate);
        entityManager.flush();

        return personToUpdate;
    }

    public void delete(Person person) {
        entityManager.remove(findByEmail(person.getEmail()));
    }
}
