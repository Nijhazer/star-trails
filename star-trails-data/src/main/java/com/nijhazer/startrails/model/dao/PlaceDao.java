package com.nijhazer.startrails.model.dao;

import com.nijhazer.startrails.model.entity.Place;

import java.util.List;

/**
 * Author: M.C. Wilson
 * Date:   10/5/12
 * Time:   2:01 PM
 */
public interface PlaceDao extends SpringDao<Place, Long> {
    public List<Place> findAll();
    public Place findByName(String name);
}
