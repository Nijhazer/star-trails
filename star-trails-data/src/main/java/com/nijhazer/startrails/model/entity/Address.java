package com.nijhazer.startrails.model.entity;

import com.nijhazer.startrails.model.entity.base.AbstractDatedEntity;
import org.hibernate.search.annotations.Field;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;

/**
 * Author: M.C. Wilson
 * Date:   10/4/12
 * Time:   11:16 AM
 */
@Entity
@Table(name="address")
@DiscriminatorValue(value="address")
public class Address extends AbstractDatedEntity implements SupportsCopy<Address> {
    private String streetNumber;
    private String route;
    private String city;
    private String county;
    private String state;
    private String nation;
    private String zipCode;
    private Double latitude;
    private Double longitude;

    @Column(name="street_number")
    @XmlElement
    public String getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    @Column(name="route")
    @XmlElement
    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    @Column(name="city")
    @XmlElement
    @Field
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Column(name="county")
    @XmlElement
    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    @Column(name="state")
    @XmlElement
    @Field
    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Column(name="nation")
    @XmlElement
    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    @Column(name="zip_code")
    @XmlElement
    @Field
    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    @Column(name="latitude")
    @XmlElement
    @Field
    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    @Column(name="longitude")
    @XmlElement
    @Field
    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public void copyFromSource(Address source) {
        copyFromSource(source, false);
    }

    @Override
    public void copyFromSource(Address source, boolean copyNullValues) {
        if (!copyNullValues) {
            source.setStreetNumber(source.getStreetNumber() == null ? this.getStreetNumber() : source.getStreetNumber());
            source.setRoute(source.getRoute() == null ? this.getRoute() : source.getRoute());
            source.setZipCode(source.getZipCode() == null ? this.getZipCode() : source.getZipCode());
            source.setCity(source.getCity() == null ? this.getCity() : source.getCity());
            source.setCounty(source.getCounty() == null ? this.getCounty() : source.getCounty());
            source.setNation(source.getNation() == null ? this.getNation() : source.getNation());
            source.setLatitude(source.getLatitude() == null ? this.getLatitude() : source.getLatitude());
            source.setLongitude(source.getLongitude() == null ? this.getLongitude() : source.getLongitude());
            source.setState(source.getState() == null ? this.getState() : source.getState());
        }
        this.setStreetNumber(source.getStreetNumber());
        this.setRoute(source.getRoute());
        this.setZipCode(source.getZipCode());
        this.setCity(source.getCity());
        this.setCounty(source.getCounty());
        this.setNation(source.getNation());
        this.setLatitude(source.getLatitude());
        this.setLongitude(source.getLongitude());
        this.setState(source.getState());
    }
}
