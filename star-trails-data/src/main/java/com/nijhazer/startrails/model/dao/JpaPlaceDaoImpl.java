package com.nijhazer.startrails.model.dao;

import com.nijhazer.startrails.model.entity.Place;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Author: M.C. Wilson
 * Date:   10/2/12
 * Time:   2:17 PM
 */
public class JpaPlaceDaoImpl implements PlaceDao {
    private static final Logger logger = LoggerFactory.getLogger(JpaPlaceDaoImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @SuppressWarnings("unchecked")
    public List<Place> findAll() {
        return entityManager
                .createQuery("SELECT a FROM Place a")
                .getResultList();
    }

    public Place findByKey(Long key) {
        logger.debug("Retrieving place with ID {}", key);
        return entityManager.find(Place.class, key);
    }

    public Place findByName(String name) {
        return (Place) entityManager
                .createQuery("SELECT a FROM Place a WHERE a.name = :name")
                .setParameter("name", name)
                .getSingleResult();
    }

    public Place create(Place place) {
        logger.debug("Creating new place: {}", place);
        entityManager.persist(place);

        return place;
    }

    public Place update(Place place) {
        Place placeToUpdate = findByKey(place.getId());

        logger.debug("Old entity: {} - New entity: {}", placeToUpdate, place);
        placeToUpdate.copyFromSource(place);

        entityManager.merge(placeToUpdate);
        entityManager.flush();

        return placeToUpdate;
    }

    public void delete(Place place) {
        entityManager.remove(findByName(place.getName()));
    }
}
