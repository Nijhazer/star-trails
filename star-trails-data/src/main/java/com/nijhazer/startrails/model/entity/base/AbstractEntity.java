package com.nijhazer.startrails.model.entity.base;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.search.annotations.DocumentId;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Author: M.C. Wilson
 * Date:   10/4/12
 * Time:   11:23 AM
 */
@Entity
@Table(name="entity_base")
@Inheritance(strategy=InheritanceType.JOINED)
@DiscriminatorColumn(name="entity_type",discriminatorType=DiscriminatorType.STRING)
@DiscriminatorValue(value="entity_base")
abstract public class AbstractEntity extends AbstractObject {
    protected Long id;

    /**
     * Returns the unique identifier associated with this JPA entity. Must be
     * overridden by derivative classes in order to provide entity-specific JPA
     * annotations for use by an ID generator.
     * @return A unique identifier associated with this entity
     */
    @Id
    @Column(name="entity_id")
    @GeneratedValue(generator="gen_entity",
            strategy=GenerationType.TABLE)
    @TableGenerator(name="gen_entity",
            table="jpa_sequence",
            pkColumnName="seq_name",
            valueColumnName="seq_count",
            pkColumnValue="entity_base",
            allocationSize=1)
    @DocumentId
    @JsonIgnore @XmlTransient
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
