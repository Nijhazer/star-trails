package com.nijhazer.startrails.model.entity;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Author: M.C. Wilson
 * Date:   10/3/12
 * Time:   9:41 AM
 */
public class ServiceResponse {
    private String message;

    public ServiceResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
