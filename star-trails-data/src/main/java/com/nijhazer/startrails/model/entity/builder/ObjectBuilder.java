package com.nijhazer.startrails.model.entity.builder;

/**
 * Author: M.C. Wilson
 * Date:   10/5/12
 * Time:   3:38 PM
 */
public interface ObjectBuilder<T> {
    public T build();
    public ObjectBuilder<T> copyFrom(T source);
}
