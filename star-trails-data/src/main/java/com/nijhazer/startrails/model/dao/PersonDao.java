package com.nijhazer.startrails.model.dao;

import com.nijhazer.startrails.model.entity.Person;

import java.util.List;

/**
 * Author: M.C. Wilson
 * Date:   10/2/12
 * Time:   2:14 PM
 */
public interface PersonDao extends SpringDao<Person, Long> {
    public Person findByEmail(String email);
    public List<Person> findByCityAndState(String city, String state);
}
