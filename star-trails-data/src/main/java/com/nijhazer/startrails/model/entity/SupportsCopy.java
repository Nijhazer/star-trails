package com.nijhazer.startrails.model.entity;

/**
 * Author: M.C. Wilson
 * Date:   10/5/12
 * Time:   3:55 PM
 */
public interface SupportsCopy<T> {
    public void copyFromSource(T source);
    public void copyFromSource(T source, boolean copyNullValues);
}
