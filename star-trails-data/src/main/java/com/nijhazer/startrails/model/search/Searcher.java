package com.nijhazer.startrails.model.search;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: nijhazer
 * Date: 10/7/12
 * Time: 11:28 AM
 * To change this template use File | Settings | File Templates.
 */
public interface Searcher<T> {
    public List<T> findByDistanceFrom(Double latitude, Double longitude, Double radius);
}
