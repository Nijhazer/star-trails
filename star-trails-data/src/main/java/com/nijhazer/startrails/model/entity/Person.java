package com.nijhazer.startrails.model.entity;

import com.nijhazer.startrails.model.entity.base.AbstractDatedEntity;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.*;

import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Author: M.C. Wilson
 * Date:   10/1/12
 * Time:   2:35 PM
 */
@Entity
@Indexed
@Table(name="person",
       uniqueConstraints={@UniqueConstraint(columnNames="email")})
@DiscriminatorValue(value="person")
@XmlRootElement(name="person")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Person extends AbstractDatedEntity implements SupportsCopy<Person> {
    private String email;
    private String firstName;
    private String lastName;
    private Address address;

    @Column(name="email")
    @NotNull @NotBlank
    @Email
    @XmlElement
    @Field
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Column(name="first_name")
    @NotNull @NotBlank
    @XmlElement
    @Field
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Column(name="last_name")
    @NotNull @NotBlank
    @XmlElement
    @Field
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @OneToOne(cascade={CascadeType.PERSIST,CascadeType.MERGE,CascadeType.REMOVE,CascadeType.REFRESH})
    @JoinColumn(name="address_id")
    @NotNull @Valid
    @XmlElement
    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public void copyFromSource(Person source) {
        copyFromSource(source, false);
    }

    public void copyFromSource(Person source, boolean copyNullValues) {
        if (!copyNullValues) {
            source.setFirstName(source.getFirstName() == null ? this.getFirstName() : source.getFirstName());
            source.setLastName(source.getLastName() == null ? this.getLastName() : source.getLastName());
        }
        this.getAddress().copyFromSource(source.getAddress(), copyNullValues);
        this.setFirstName(source.getFirstName());
        this.setLastName(source.getLastName());
    }
}
