package com.nijhazer.startrails.model.dao;

import java.util.List;

/**
 * Author: M.C. Wilson
 * Date:   10/5/12
 * Time:   2:18 PM
 */
public interface Dao<T, K> {
    public List<T> findAll();
    public T findByKey(K key);
    public T create(T object);
    public T update(T object);
    public void delete(T object);
}
