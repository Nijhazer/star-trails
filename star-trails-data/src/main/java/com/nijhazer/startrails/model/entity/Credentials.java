package com.nijhazer.startrails.model.entity;

import com.nijhazer.startrails.model.entity.base.AbstractDatedEntity;

import javax.persistence.*;

/**
 * Author: M.C. Wilson
 * Date:   10/1/12
 * Time:   2:35 PM
 */
@Entity
@Table(name="credentials")
@DiscriminatorValue(value="credentials")
public class Credentials extends AbstractDatedEntity {
    private String guid;
    private String password;

    @Column(name="guid")
    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    @Column(name="password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
