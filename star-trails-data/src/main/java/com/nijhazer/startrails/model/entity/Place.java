package com.nijhazer.startrails.model.entity;

import com.nijhazer.startrails.model.entity.base.AbstractDatedEntity;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.Spatial;
import org.hibernate.search.annotations.SpatialMode;
import org.hibernate.search.spatial.Coordinates;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.*;

/**
 * Author: M.C. Wilson
 * Date:   10/3/12
 * Time:   2:07 PM
 */
@Entity
@Indexed
@Spatial(name="place",spatialMode=SpatialMode.GRID)
@Table(name="place",
       uniqueConstraints={@UniqueConstraint(columnNames="name")})
@DiscriminatorValue(value="place")
@XmlRootElement(name="place")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Place extends AbstractDatedEntity implements SupportsCopy<Place>, Coordinates {
    private String name;
    private String description;
    private Address address;

    @Column(name="name")
    @NotNull @NotBlank
    @XmlElement
    @Field
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name="description")
    @XmlElement
    @Field
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @OneToOne(cascade={CascadeType.PERSIST,CascadeType.MERGE,CascadeType.REMOVE,CascadeType.REFRESH})
    @JoinColumn(name="address_id")
    @NotNull @Valid
    @XmlElement
    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public void copyFromSource(Place source) {
        copyFromSource(source, false);
    }

    public void copyFromSource(Place source, boolean copyNullValues) {
        if (!copyNullValues) {
            source.setDescription(source.getDescription() == null ? this.getDescription() : source.getDescription());
        }
        this.getAddress().copyFromSource(source.getAddress(), copyNullValues);
        this.setDescription(source.getDescription());
    }

    @Transient @XmlTransient @JsonIgnore
    public Double getLatitude() {
        return getAddress().getLatitude();
    }

    @Transient @XmlTransient @JsonIgnore
    public Double getLongitude() {
        return getAddress().getLongitude();
    }
}
