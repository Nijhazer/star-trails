package com.nijhazer.startrails.model.manager;

import com.nijhazer.startrails.model.dao.PersonDao;
import com.nijhazer.startrails.model.entity.Person;
import com.nijhazer.startrails.model.validator.PersonValidator;
import com.nijhazer.startrails.util.formatter.AddressFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Author: M.C. Wilson
 * Date:   10/4/12
 * Time:   12:32 PM
 */
public class PersonManager {
    private static final Logger logger = LoggerFactory.getLogger(PersonManager.class);
    private PersonValidator validator;
    private AddressFormatter formatter;
    private PersonDao personDao;

    public Person findByEmail(String email) {
        return personDao.findByEmail(email);
    }

    public List<Person> findAll() {
       return personDao.findAll();
    }

    public Person findByKey(Long key) {
        return personDao.findByKey(key);
    }

    public Person create(Person person, String address) {
        person.setAddress(formatter.fromSingleAddressString(address));
        validator.validate(person);
        return create(person);
    }

    public Person create(Person person) {
        validator.validate(person);
        return personDao.create(person);
    }

    public Person update(Person person, String address) {
        person.setAddress(formatter.fromSingleAddressString(address));
        return update(person);
    }

    public Person update(Person person) {
        Person personToUpdate = findByEmail(person.getEmail());
        personToUpdate.copyFromSource(person);

        validator.validate(person);
        return personDao.update(person);
    }

    public void delete(Person person) {
        personDao.delete(person);
    }

    public void setFormatter(AddressFormatter formatter) {
        this.formatter = formatter;
    }

    public void setPersonDao(PersonDao personDao) {
        this.personDao = personDao;
    }

    public void setValidator(PersonValidator validator) {
        this.validator = validator;
    }
}
