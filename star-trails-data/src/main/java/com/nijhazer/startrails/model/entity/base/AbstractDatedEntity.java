package com.nijhazer.startrails.model.entity.base;

import org.codehaus.jackson.annotate.JsonIgnore;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlTransient;
import java.util.Date;

/**
 * Author: M.C. Wilson
 * Date:   10/4/12
 * Time:   11:22 AM
 */
@Entity
@Table(name="entity_base_date")
@DiscriminatorValue(value="entity_base_date")
abstract public class AbstractDatedEntity extends AbstractEntity {
    private Date creationDate;
    private Date lastUpdatedDate;
    private Date expirationDate;

    /**
     * The creation date should correspond to the date that this entity was first created
     * in the database.
     * @return A Date object representing the creation date of this entity
     */
    @Column(name = "creation_date")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonIgnore @XmlTransient
    public final Date getCreationDate() {
        return creationDate;
    }

    /**
     * The creation date should correspond to the date that this entity was first created
     * in the database.
     * @param creationDate A Date object representing the creation date of this entity
     */
    public final void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * The last updated date should correspond to the date that this entity was last updated
     * in the database.
     * @return A Date object representing the date when this entity was last updated
     */
    @Column(name = "last_updated_date")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonIgnore @XmlTransient
    public final Date getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    /**
     * The last updated date should correspond to the date that this entity was last updated
     * in the database.
     * @param lastUpdatedDate A Date object representing the date when this entity was last updated
     */
    public final void setLastUpdatedDate(Date lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    /**
     * The expiration date should indicate the date after which this entity should no longer be included
     * in the standard process flow.
     * @return A Date object representing the expiration date
     */
    @Column(name = "expiration_date")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonIgnore @XmlTransient
    public final Date getExpirationDate() {
        return expirationDate;
    }

    /**
     * The expiration date should indicate the date after which this entity should no longer be included
     * in the standard process flow.
     * @param expirationDate A Date object representing the expiration date
     */
    public final void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    /**
     * Method for setting the creation date value to the current date.
     * Intended to be triggered by JPA when adding a new entity.
     */
    @PrePersist
    public void onCreate() {
        setCreationDate(new Date());
    }

    /**
     * Method for setting the updated date value to the current date.
     * Intended to be triggered by JPA when updating an entity.
     */
    @PreUpdate
    public void onUpdate() {
        setLastUpdatedDate(new Date());
    }
}
