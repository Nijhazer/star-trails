package com.nijhazer.startrails.constant;

/**
 * Author: M.C. Wilson
 * Date:   10/5/12
 * Time:   10:39 AM
 */
public class StringFormattingConstants {
    public static final String NEWLINE = System.getProperty("line.separator");
}
