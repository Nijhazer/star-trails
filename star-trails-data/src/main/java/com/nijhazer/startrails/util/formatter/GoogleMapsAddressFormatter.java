package com.nijhazer.startrails.util.formatter;

import com.nijhazer.startrails.constant.StringFormattingConstants;
import com.nijhazer.startrails.convert.googlemaps.geocode.model.AddressComponent;
import com.nijhazer.startrails.convert.googlemaps.geocode.model.GMGResponse;
import com.nijhazer.startrails.convert.googlemaps.geocode.model.GMGResult;
import com.nijhazer.startrails.model.entity.Address;
import com.nijhazer.startrails.model.entity.builder.AddressBuilder;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import java.util.*;

/**
 * Author: M.C. Wilson
 * Date:   10/5/12
 * Time:   10:31 AM
 */
public class GoogleMapsAddressFormatter implements AddressFormatter {
    private static final Logger logger = LoggerFactory.getLogger(GoogleMapsAddressFormatter.class);
    private String webResourceUrl;
    private WebResource webResource;
    private String sensor;
    private String path;
    private MediaType mediaType;
    private List<String> targetAddressComponents;
    private Set<String> ignoreAddressComponents;

    public void setWebResource(WebResource webResource) {
        this.webResource = webResource;
    }

    public Address buildRequest(Map<String, String> queryParameters) {
        logger.debug("Issuing request to Google Maps to format address - parameters: {}{}WebResource URL: {}{}Sensor: {}{}Path: {}{}MediaType: {}{}",
            new Object[]{
                queryParameters,
                StringFormattingConstants.NEWLINE,
                webResourceUrl,
                StringFormattingConstants.NEWLINE,
                sensor,
                StringFormattingConstants.NEWLINE,
                path,
                StringFormattingConstants.NEWLINE,
                mediaType,
                StringFormattingConstants.NEWLINE
            }
        );
        MultivaluedMap<String, String> paramMap = new MultivaluedMapImpl();
        for (Map.Entry<String, String> queryParameter : queryParameters.entrySet()) {
            paramMap.putSingle(queryParameter.getKey(), queryParameter.getValue());
        }
        return convertToStandardAddress(webResource
                .queryParams(paramMap)
                .queryParam("sensor", sensor)
                .path(path)
                .accept(mediaType)
                .get(GMGResponse.class));
    }

    public Address fromSingleAddressString(String address) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("address", address);
        return buildRequest(params);
    }

    public Address fromLatitudeAndLongitude(Double latitude, Double longitude) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("latlng", latitude.toString() + "," + longitude.toString());
        return buildRequest(params);
    }

    private Address convertToStandardAddress(GMGResponse googleMapsAddressData) {
        logger.debug("Converting GM response to Address - response: {}", googleMapsAddressData);

        GMGResult result = null;
        String selectedAddressComponent = "";
        for (String targetAddressComponent : targetAddressComponents) {
            selectedAddressComponent = targetAddressComponent;
            for (GMGResult iterResult : googleMapsAddressData.getResults()) {
                if (iterResult.getTypes().contains(targetAddressComponent)) {
                    result = iterResult;
                    break;
                }
            }
            if (result != null) {
                break;
            }
        }

        logger.debug("Selected GM result, from component {}: {}", selectedAddressComponent, result);
        List<AddressComponent> addressComponents = result.getAddressComponents();

        Map<String, String> addressComponentMap = new HashMap<String, String>();
        for (AddressComponent addressComponent : addressComponents) {
            String addressComponentType = "";
            for (String proposedAddressComponentType : addressComponent.getTypes()) {
                if (!ignoreAddressComponents.contains(proposedAddressComponentType)) {
                    addressComponentType = proposedAddressComponentType;
                }
            }
            if (!StringUtils.isEmpty(addressComponentType)) {
                addressComponentMap.put(addressComponentType, addressComponent.getLongName());
            }
        }

        Address returnAddress = new AddressBuilder()
                .streetNumber(addressComponentMap.get("street_number"))
                .route(addressComponentMap.get("route"))
                .city(addressComponentMap.get("locality"))
                .county(addressComponentMap.get("administrative_area_level_2"))
                .state(addressComponentMap.get("administrative_area_level_1"))
                .zipCode(addressComponentMap.get("postal_code"))
                .nation(addressComponentMap.get("country"))
                .latitude(result.getGeometry().getLocation().getLatitude())
                .longitude(result.getGeometry().getLocation().getLongitude())
                .build();
        logger.debug("Converted to Address: {}", returnAddress);
        return returnAddress;
    }

    public String getWebResourceUrl() {
        return webResourceUrl;
    }

    public void setWebResourceUrl(String webResourceUrl) {
        this.webResourceUrl = webResourceUrl;
    }

    public String getSensor() {
        return sensor;
    }

    public void setSensor(String sensor) {
        this.sensor = sensor;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public MediaType getMediaType() {
        return mediaType;
    }

    public void setMediaType(MediaType mediaType) {
        this.mediaType = mediaType;
    }

    public Set<String> getIgnoreAddressComponents() {
        return ignoreAddressComponents;
    }

    public void setIgnoreAddressComponents(Set<String> ignoreAddressComponents) {
        this.ignoreAddressComponents = ignoreAddressComponents;
    }

    public void setTargetAddressComponents(List<String> targetAddressComponents) {
        this.targetAddressComponents = targetAddressComponents;
    }
}
