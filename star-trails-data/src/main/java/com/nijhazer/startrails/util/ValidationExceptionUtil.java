package com.nijhazer.startrails.util;

import com.nijhazer.startrails.constant.StringFormattingConstants;

import javax.validation.ConstraintViolation;
import java.util.Set;

/**
 * Author: M.C. Wilson
 * Date:   10/2/12
 * Time:   3:31 PM
 */
public class ValidationExceptionUtil<T> {
    public String format(Set<ConstraintViolation<T>> violations) {
        StringBuilder builder = new StringBuilder();
        builder.append("(")
               .append(violations.size())
               .append(") validation error(s) were found.")
               .append(StringFormattingConstants.NEWLINE);
        int violationCount = 0;
        for (ConstraintViolation<T> violation : violations) {
            violationCount++;
            builder.append(violationCount)
                   .append(": '")
                   .append(violation.getPropertyPath())
                   .append("' ")
                   .append(violation.getMessage())
                   .append(StringFormattingConstants.NEWLINE);
        }
        return builder.toString();
    }
}
