package com.nijhazer.startrails.util.formatter;

import com.nijhazer.startrails.model.entity.Address;

/**
 * Author: M.C. Wilson
 * Date:   10/5/12
 * Time:   10:29 AM
 */
public interface AddressFormatter {
    public Address fromSingleAddressString(String address);
    public Address fromLatitudeAndLongitude(Double latitude, Double longitude);
}
