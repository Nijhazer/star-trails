package com.nijhazer.startrails.util;

import javax.validation.Configuration;
import javax.validation.Validation;
import javax.validation.ValidatorFactory;

/**
 * Author: M.C. Wilson
 * Date:   10/2/12
 * Time:   2:44 PM
 */
public class ValidatorFactoryBuilder {
    public static ValidatorFactory buildValidatorFactory() {
        Configuration<?> configuration = Validation.byDefaultProvider().configure();
        return configuration.buildValidatorFactory();
    }
}
