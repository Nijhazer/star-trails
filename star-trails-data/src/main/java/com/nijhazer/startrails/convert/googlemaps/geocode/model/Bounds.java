package com.nijhazer.startrails.convert.googlemaps.geocode.model;

import com.nijhazer.startrails.model.entity.base.AbstractObject;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Author: M.C. Wilson
 * Date:   10/4/12
 * Time:   3:05 PM
 */
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Bounds extends AbstractObject {
    private Coordinate southwest;
    private Coordinate northeast;

    @XmlElement(name="southwest")
    public Coordinate getSouthwest() {
        return southwest;
    }

    public void setSouthwest(Coordinate southwest) {
        this.southwest = southwest;
    }

    @XmlElement(name="northeast")
    public Coordinate getNortheast() {
        return northeast;
    }

    public void setNortheast(Coordinate northeast) {
        this.northeast = northeast;
    }
}
