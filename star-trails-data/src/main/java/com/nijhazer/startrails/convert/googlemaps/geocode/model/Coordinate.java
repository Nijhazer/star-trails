package com.nijhazer.startrails.convert.googlemaps.geocode.model;

import com.nijhazer.startrails.model.entity.base.AbstractObject;
import org.hibernate.search.spatial.Coordinates;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Author: M.C. Wilson
 * Date:   10/4/12
 * Time:   1:29 PM
 */
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Coordinate extends AbstractObject implements Coordinates {
    private Double latitude;
    private Double longitude;

    @XmlElement(name="lat")
    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    @XmlElement(name="lng")
    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }
}
