package com.nijhazer.startrails.convert.googlemaps.geocode.model;

import com.nijhazer.startrails.model.entity.base.AbstractObject;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.Set;

/**
 * Author: M.C. Wilson
 * Date:   10/4/12
 * Time:   1:22 PM
 */
@XmlRootElement(name="result")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class GMGResult extends AbstractObject {
    private Set<String> types;
    private List<AddressComponent> addressComponents;
    private String formattedAddress;
    private Geometry geometry;

    @XmlElement(name="type")
    public Set<String> getTypes() {
        return types;
    }

    public void setTypes(Set<String> types) {
        this.types = types;
    }

    @XmlElement(name="address_component")
    public List<AddressComponent> getAddressComponents() {
        return addressComponents;
    }

    public void setAddressComponents(List<AddressComponent> addressComponents) {
        this.addressComponents = addressComponents;
    }

    @XmlElement(name="formatted_address")
    public String getFormattedAddress() {
        return formattedAddress;
    }

    public void setFormattedAddress(String formattedAddress) {
        this.formattedAddress = formattedAddress;
    }

    @XmlElement
    public Geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }
}
