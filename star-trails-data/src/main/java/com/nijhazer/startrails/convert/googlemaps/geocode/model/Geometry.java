package com.nijhazer.startrails.convert.googlemaps.geocode.model;

import com.nijhazer.startrails.model.entity.base.AbstractObject;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Author: M.C. Wilson
 * Date:   10/4/12
 * Time:   1:27 PM
 */
@XmlRootElement(name="geometry")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Geometry extends AbstractObject {
    private Bounds viewport;
    private Coordinate location;
    private String locationType;
    private Bounds bounds;

    @XmlElement(name="viewport")
    public Bounds getViewport() {
        return viewport;
    }

    public void setViewport(Bounds viewport) {
        this.viewport = viewport;
    }

    @XmlElement(name="location")
    public Coordinate getLocation() {
        return location;
    }

    public void setLocation(Coordinate location) {
        this.location = location;
    }

    @XmlElement(name="location_type")
    public String getLocationType() {
        return locationType;
    }

    public void setLocationType(String locationType) {
        this.locationType = locationType;
    }

    @XmlElement(name="bounds")
    public Bounds getBounds() {
        return bounds;
    }

    public void setBounds(Bounds bounds) {
        this.bounds = bounds;
    }
}
