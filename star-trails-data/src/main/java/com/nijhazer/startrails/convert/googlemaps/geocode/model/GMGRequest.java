package com.nijhazer.startrails.convert.googlemaps.geocode.model;

import com.nijhazer.startrails.model.entity.base.AbstractObject;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Author: M.C. Wilson
 * Date:   10/4/12
 * Time:   1:52 PM
 */
@XmlRootElement(name="request")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class GMGRequest extends AbstractObject {
    private String address;
    private String sensor;

    public GMGRequest() {}

    public GMGRequest(String address) {
        setSensor("false");
        setAddress(address);
    }

    @XmlElement(name="address")
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @XmlElement(name="sensor")
    public String getSensor() {
        return sensor;
    }

    public void setSensor(String sensor) {
        this.sensor = sensor;
    }
}
