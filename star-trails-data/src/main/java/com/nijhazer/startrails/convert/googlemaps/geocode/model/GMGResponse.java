package com.nijhazer.startrails.convert.googlemaps.geocode.model;

import com.nijhazer.startrails.model.entity.base.AbstractObject;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Author: M.C. Wilson
 * Date:   10/4/12
 * Time:   1:20 PM
 */
@XmlRootElement(name="GeocodeResponse")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class GMGResponse extends AbstractObject {

    private List<GMGResult> results;
    private String status;

    @XmlElement(name="result")
    public List<GMGResult> getResults() {
        return results;
    }

    public void setResults(List<GMGResult> results) {
        this.results = results;
    }

    @XmlElement(name="status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
