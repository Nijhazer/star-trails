package com.nijhazer.startrails.convert.googlemaps.geocode.model;

import com.nijhazer.startrails.model.entity.base.AbstractObject;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Author: M.C. Wilson
 * Date:   10/4/12
 * Time:   1:25 PM
 */
@XmlRootElement(name="address_component")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class AddressComponent extends AbstractObject {
    private List<String> types;
    private String longName;
    private String shortName;

    @XmlElement(name="type")
    public List<String> getTypes() {
        return types;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }

    @XmlElement(name="long_name")
    public String getLongName() {
        return longName;
    }

    public void setLongName(String longName) {
        this.longName = longName;
    }

    @XmlElement(name="short_name")
    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }
}
