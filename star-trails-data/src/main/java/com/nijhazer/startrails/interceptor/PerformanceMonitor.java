package com.nijhazer.startrails.interceptor;

import com.nijhazer.startrails.model.pojo.MethodStat;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Author: M.C. Wilson
 * Date:   10/23/12
 * Time:   10:38 PM
 */
public class PerformanceMonitor implements MethodInterceptor {
    private static final Logger logger = LoggerFactory.getLogger(PerformanceMonitor.class);

    public Object invoke(MethodInvocation method) throws Throwable {
        MethodStat stat = new MethodStat(method.getMethod().getDeclaringClass().getName() + "." + method.getMethod().getName());
        stat.start();
        try {
            return method.proceed();
        } finally {
            stat.finish();
            logger.trace("{}", stat);
        }
    }
}